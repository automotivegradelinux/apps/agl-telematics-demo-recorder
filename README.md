# Telematics Recorder

## Overview

The telematics recorder demonstation application reads the OBD-II vehicle speed and
engine speed (RPM) messages using the low-level CAN binding, and sends them as JSON
messages to a MQTT broker for clients to use.

## Configuration

The application has several configurable parameters, which can be configured via the
INI style file /etc/xdg/telematics-recorder.conf. The parameters are described in the
following sections.

### General

General configuration consists of the following parameters in the "[General]" section:
* log_level
* cellular_enabled
* gps_enabled
* check_online
* device_uuid

#### log_level

Sets the debug logging level, acceptable values are 1 or higher. If the parameter is not
present, logging is disabled. The default value is no debug logging.

#### cellular_enabled

Enables or disables cellular modem usage, acceptable values are "true" and "false" (case-insensitive).
The logic enabled by this parameter blocks on a check for cellular modem presence at start up, and
sets the modem state to online when detected. The default value is disabled ("false").

#### gps_enabled

Enables or disables GPS usage, acceptable values are "true" and "false" (case-insensitive).
When enabled, the current GPS position will be populated in the events sent to the MQTT broker.
The default value is disabled ("false").

#### check_online

Enables or disables checking for online status, acceptable values are "true" and "false" (case-insensitive).
When enabled, the the current network online status will be checked before attempting to send messages to
the MQTT broker to potentially prevent filling the transmit queue of the MQTT client.
The default value is disabled ("false").

#### device_uuid

A string representing a unique device identifier. It is used to identify the device in the MQTT messages.
If not specified, the default value is "e4bbc0a8-f435-4326-9769-d4a2c9f3c18d".

### MQTT

MQTT protocol configuration consists of the following parameters in the "[MQTT]" section:
* broker
* port
* keepalive
* qos
* retain
* username
* password

#### broker

MQTT broker hostname. Default value is "iot.eclipse.org".

#### port

MQTT broker port. Default value is 1883.

#### keepalive

MQTT protocol keepalive time. Default value is 60 seconds.

#### qos

MQTT protocol quality of service (QoS) parameter, acceptable values are 0, 1, or 2.
Default value is 0. See "Quality of Service" in [https://mosquitto.org/man/mqtt-7.html](https://mosquitto.org/man/mqtt-7.html)
for a description of the QoS settings.

#### retain

MQTT protocol message retain parameter, acceptable values are "true" and "false" (case-insensitive). 
Default value is "true". See "Retained Messages" in [https://mosquitto.org/man/mqtt-7.html](https://mosquitto.org/man/mqtt-7.html)
for a description of the message retention mechanism.

#### username

MQTT broker username string if required. Default value is none.

#### password

MQTT broker password string if required. Default value is none, and only relevant in conjunction
with the username parameter.

### Event

Event configuration consists of the following parameters in the "[Event]" section:
* update_period

#### update_period

The period in seconds for sending messages to the MQTT broker, acceptable values are 0 or higher.
A value of 0 means send a message to the broker for every CAN event that comes in. The default value is 10.
